package com.test.falabella.repository;

import com.test.falabella.entities.Product;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
@Mapper
public interface ProductRepository extends JpaRepository<Product, String> {}
