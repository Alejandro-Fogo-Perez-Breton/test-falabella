package com.test.falabella.services;

import com.test.falabella.entities.Product;

import java.util.List;

public interface ProductServices{

    List<Product> getAllProduct();

    Product getProductBySku(String sku) throws Exception;

    Product createProduct(Product product);

    void deleteProduct(String sku);


}
