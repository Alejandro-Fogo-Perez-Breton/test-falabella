package com.test.falabella.services.impl;

import com.test.falabella.entities.Product;
import com.test.falabella.repository.ProductRepository;
import com.test.falabella.services.ProductServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class ProductImpl implements ProductServices {

  @Autowired private ProductRepository crudRepositoryServices;

  @Override
  public List<Product> getAllProduct() {

    List<Product> productsResponses = new ArrayList<Product>();

    crudRepositoryServices.findAll().forEach(productsResponses::add);

    return productsResponses;
  }

  @Override
  public Product getProductBySku(String sku) throws Exception {

    return crudRepositoryServices.findById(sku).get();
  }

  @Override
  public Product createProduct(Product product) {
    return crudRepositoryServices.save(product);
  }

  @Override
  public void deleteProduct(String sku) {
    crudRepositoryServices.deleteById(sku);
  }
}
