package com.test.falabella.controller;

import com.test.falabella.entities.Product;
import com.test.falabella.services.ProductServices;
import org.aspectj.apache.bcel.classfile.Module;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/products")
public class ProductController {

  @Autowired ProductServices productServices;

  @GetMapping()
  public ResponseEntity<List<Product>> getAllProduct() {

    return ResponseEntity.ok(productServices.getAllProduct());
  }

  @GetMapping("/{sku}")
  public ResponseEntity<Product> getProductBySku(@PathVariable("sku") String sku) throws Exception {
    return ResponseEntity.ok(productServices.getProductBySku(sku));
  }

  @PostMapping("/create")
  public ResponseEntity<Product> createProduct(@RequestBody Product product) {

    return ResponseEntity.ok(productServices.createProduct(product));
  }

  @DeleteMapping("/delete/{sku}")
  public void deleteProduct(@PathVariable("sku") String sku) {

    productServices.deleteProduct(sku);
  }
}
