package com.test.falabella.entities;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.Hibernate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "products")
@Getter
@Setter
@ToString
@RequiredArgsConstructor
@AllArgsConstructor
public class Product implements Serializable {

  private static final long serialVersionUID = 1L;

  @Id
  @Column(name = "sku", nullable = false)

  private String sku;

  private String name;

  private String brand;

  private String sizeProduct;

  private String price;

  private String principalImage;

  private String otherImages;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || Hibernate.getClass(this) != Hibernate.getClass(o)) return false;
    Product that = (Product) o;
    return sku != null && Objects.equals(sku, that.sku);
  }

  @Override
  public int hashCode() {
    return getClass().hashCode();
  }
}
